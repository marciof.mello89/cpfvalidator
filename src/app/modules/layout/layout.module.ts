import { SharedModule } from './../../shared/shared/shared.module';
import { NgModule } from '@angular/core';
import { SidenavComponent } from './sidenav/sidenav.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    SidenavComponent,
    ToolbarComponent
  ],
  imports: [
    SharedModule,
    RouterModule
  ],
  exports: [
    SidenavComponent,
    ToolbarComponent
  ]
})
export class LayoutModule { }
