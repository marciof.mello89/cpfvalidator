import { AlertaToastrService } from './../../../shared/services/alertaToastr.service';
import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'app-cpf-validation',
  templateUrl: './cpf-validation.component.html',
  styleUrls: ['./cpf-validation.component.scss']
})
export class CpfValidationComponent implements OnInit {
  firstFormGroup = this._formBuilder.group({
    cpf: ['', Validators.required],
  });
  show = false;

  constructor(private _formBuilder: FormBuilder,
              private alertaToastr: AlertaToastrService) {}

  ngOnInit(): void {
  }

  get formControls() {
    return this.firstFormGroup.controls;
  }

  consultar() {
    console.log(this.formControls['cpf'].value);
    if(this.formControls['cpf'].value == '11111111111') {
      this.show = true;
    } else {
      const msg = 'CPF não encontrado!'
      this.show = false;
      this.alertaToastr.showToasterError(msg, 'Erro');
    }
  }

}
