import { SharedModule } from './../../shared/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CpfValidationComponent } from './cpf-validation/cpf-validation.component';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main.component';
import { DetailedInfoComponent } from './detailed-info/detailed-info.component';

const routes: Routes = [
  {
    path: 'cpf-validation',
    component: CpfValidationComponent
  },
  {
    path: 'home',
    component: MainComponent
  }
];


@NgModule({
  declarations: [
    CpfValidationComponent,
    MainComponent,
    DetailedInfoComponent,
  ],
  imports: [
    RouterModule.forRoot(routes),
    CommonModule,
    SharedModule
  ],
  exports: [
    CpfValidationComponent,
    MainComponent,
    DetailedInfoComponent,
  ]
})
export class MainModule { }
