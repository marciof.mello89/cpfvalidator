import { ToastrService } from 'ngx-toastr';
import { Injectable } from '@angular/core';

@Injectable()
export class AlertaToastrService {

  constructor(private toastrService: ToastrService) { }

  showToasterSuccess(mensagem: string, titulo: string) {
    this.toastrService.success(mensagem, titulo, {
      timeOut: 30000,
      progressBar: true,
      extendedTimeOut: 30000,
      closeButton: true,
      positionClass: 'toast-center-center'

    });
  }

  showToasterError(mensagem: string, titulo: string) {
    this.toastrService.error(mensagem, titulo, {
      disableTimeOut: true,
      progressBar: false,
      closeButton: true,
      positionClass: 'toast-center-center'

    });
  }


  showToasterWarning(mensagem: string, titulo: string) {
    this.toastrService.warning(mensagem, titulo, {
      closeButton: true,
      progressBar: true,
      positionClass: 'toast-center-center',
      timeOut: 30000,
      extendedTimeOut: 30000
    });
  }


}
